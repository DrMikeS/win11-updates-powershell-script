Write-Host "Setting up the Execution Policy...!"
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser -Force
Write-Host "Execution Policy Setted up\nGetting Application that need updates..."

# Install Winget (if not already installed)
if (!(Get-Command winget.exe -ErrorAction SilentlyContinue)) {
    Invoke-WebRequest -Uri "https://github.com/microsoft/winget-cli/releases/download/v1.0.11692/Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.appxbundle" -OutFile "$env:TEMP\winget.appxbundle"
    Add-AppxPackage -Path "$env:TEMP\winget.appxbundle"
}


# List of application processes to close
$appsToClose = "Teams", "firefox", "chrome", "7zFM", "AcroRd32", "msedge", "vlc", "TeamsForEducation", "Zoom"

# Loop through the processes and close them
foreach ($app in $appsToClose) {
    $processes = Get-Process -Name $app -ErrorAction SilentlyContinue
    if ($processes) {
        $processes | Stop-Process -Force
    }
}


# Update applications using Winget
winget upgrade --all --accept-package-agreements --accept-source-agreements --silent

#Upgrade Teams Machine-Wide Installer Explicity
winget update -id=Microsoft.TeamInstaller --exact --accept-package-agreements --accept-source-agreements


# Update Windows using PSWindowsUpdate
Write-Host "Installing PSWindows Update..."
Install-PackageProvider NuGet -Force -Confirm:$false
Install-Module -Name PSWindowsUpdate -Force -Confirm:$false
Import-Module PSWindowsUpdate
Write-Host "Starting Windows Update..."

# Check for missing Windows updates
$updates = Get-WindowsUpdate

 #If there are updates available, install them
if ($updates.Count -gt 0) {
    
        do {
        # Install updates and wait for the computer to reboot
        Write-Host "Installing Windows updates..."
        Install-WindowsUpdate -AcceptAll

        # Wait for the computer to restart
        Write-Host "Rebooting ..."

        # Check for pending updates
        $updates = Get-WindowsUpdate

    } while ($updates.Count -gt 0)

}
else {
    Write-Host "No updates found."
}


Install-WindowsUpdate -AcceptAll -AutoReboot

# Reboot the local computer
Restart-Computer

# Get the current script process ID
$scriptProcessId = $PID

# Terminate the current script process
Stop-Process -Id $scriptProcessId -Force